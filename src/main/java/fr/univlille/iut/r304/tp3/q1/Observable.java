package fr.univlille.iut.r304.tp3.q1;


public abstract class Observable {

	protected void notifyObservers() {
	}

	protected void notifyObservers(Object data) {
	}

	public void attach(Observer observer) {
	}

	public void detach(Observer observer) {
	}
}
