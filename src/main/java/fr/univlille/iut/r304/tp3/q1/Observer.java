package fr.univlille.iut.r304.tp3.q1;

public interface Observer {

  void update(Observable subj);
  void update(Observable subj, Object data);

}
