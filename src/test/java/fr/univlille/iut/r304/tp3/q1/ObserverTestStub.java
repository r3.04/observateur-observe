package fr.univlille.iut.r304.tp3.q1;

/** ObserverTestStub
 * Can tell whether it was notified or not
 * Have a value that can be set by notification
 */
public class ObserverTestStub implements Observer {

    protected boolean wasNotified;

    protected int someValue;
    protected static final int expectedValue = 42;

    public ObserverTestStub() {
        wasNotified = false;
        someValue = 0;
    }

    public int getValue() {
        return someValue;
    }

    public int getExpectedValue() {
        return expectedValue;
    }

    public void setValue(int someValue) {
        this.someValue = someValue;
    }

    public void update(Observable subj) {
        wasNotified = true;
    }

    public void update(Observable subj, Object data) {
        wasNotified = true;
        someValue = (int) data;
        if (someValue == expectedValue){
            subj.detach(this);
        }
    }

    public boolean wasNotified() {
        return wasNotified;
    }

}
