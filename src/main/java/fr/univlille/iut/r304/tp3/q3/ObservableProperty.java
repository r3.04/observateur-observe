package fr.univlille.iut.r304.tp3.q3;

import fr.univlille.iut.r304.tp3.q1.Observer;

public class ObservableProperty {

	public void setValue(Object i) {
	}

	public Object getValue() {
		return null;
	}

	public void attach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
		// n'est pas censée rester une fois que vous avez fini Q2.1
	}

	public void detach(Observer observer) {
		// methode cree pour que les tests compilent sans erreur
		// n'est pas censée rester une fois que vous avez fini Q2.1
	}

}
